package zti.ksiegarnia.ejb;

import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import zti.ksiegarnia.entity.Book;
import zti.ksiegarnia.web.managedbeans.ShoppingCart;

public class BookRequestBeanTest {
    @Mock
    private EntityManager em;
 
    public BookRequestBeanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createBook method, of class BookRequestBean.
     */
    @Test
    public void testCreateBook() throws Exception {
        System.out.println("createBook");
        String bookId = "123";
        String surname = "Wasek";
        String firstname = "Michal";
        String title = "Test";
        Double price = 123.0;
        Boolean onsale = true;
        Integer calendarYear = 2013;
        String description = "opis";
        Integer inventory = 20;
        
        BookRequestBean instance = new BookRequestBean();
        instance.setEntityManager(em);
        instance.createBook(bookId, surname, firstname, title, price, onsale, calendarYear, description, inventory);
        Book book = new Book(bookId, surname, firstname, title, price, onsale, calendarYear, description, inventory);
        assertEquals(instance.getBook("123"), book);
    }
   /**
     * Test of getBooks method, of class BookRequestBean.
     */
    @Test
    public void testGetBooks() throws Exception {
        System.out.println("getBooks");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BookRequestBean instance = (BookRequestBean)container.getContext().lookup("java:global/classes/BookRequestBean");
        List<Book> result = instance.getBooks();
        container.close();
		assertNotNull(result);
    }

    /**
     * Test of getBook method, of class BookRequestBean.
     */
    @Test
    public void testGetBook() throws Exception {
        System.out.println("getBook");
        String bookId = "";
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BookRequestBean instance = (BookRequestBean)container.getContext().lookup("java:global/classes/BookRequestBean");
        Book expResult = null;
        Book result = instance.getBook(bookId);
        assertEquals(expResult, result);
        container.close();
    }

    /**
     * Test of buyBooks method, of class BookRequestBean.
     */
    @Test
    public void testBuyBooks() throws Exception {
        System.out.println("buyBooks");
        ShoppingCart cart = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BookRequestBean instance = (BookRequestBean)container.getContext().lookup("java:global/classes/BookRequestBean");
        instance.buyBooks(cart);
        container.close();
    }

    
}
