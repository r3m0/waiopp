package zti.ksiegarnia.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class ConfigBean {

    @EJB
    private BookRequestBean request;

    @PostConstruct
    public void createData() {
        request.createBook("201", "Walls", "",
                "Spring w Akcji. Wydanie III",
                30.75, false, 2005, "Super książka.", 20);
        request.createBook("202", "Martin", "",
                "Czysty kod. Podręcznik dobrego programisty", 40.75, true,
                2010, "Super książka.", 20);
        request.createBook("203", "Robert", "Love",
                "Jądro Linuksa. Przewodnik programisty",
                27.75, false, 2010, "Super książka.", 20);
        request.createBook("205", "Loeliger", "Matthew McCullough",
                "Kontrola wersji z systemem Git. Narzędzia i techniki programistów. Wydanie II",
                10.75, true, 2008, "Super książka.", 20);
        request.createBook("206", "Hunt", "Dave Thomas",
                "JUnit. Pragmatyczne testy jednostkowe w Javie",
                30.00, true, 2008, "Super książka.", 20);
        request.createBook("207", "Howard", "Goldberg",
                "XML. Szybki start. Wydanie II", 30.95, true,
                2010, "Super książka.", 20);

    }
}
