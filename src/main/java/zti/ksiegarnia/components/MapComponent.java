package zti.ksiegarnia.components;

import zti.ksiegarnia.listeners.AreaSelectedEvent;
import javax.faces.component.FacesComponent;
import javax.faces.component.UICommand;

@FacesComponent("DemoMap")
public class MapComponent extends UICommand {

    private enum PropertyKeys {
        current;
    }
    private final String current = null;

    public MapComponent() {
        super();
    }

    public String getCurrent() {
        return (String) getStateHelper().eval(PropertyKeys.current, null);
    }

    public void setCurrent(String current) {
        if (this.getParent() == null) {
            return;
        }

        String previous = (String) getStateHelper().get(current);
        getStateHelper().put(PropertyKeys.current, current);

        if ((previous == null) && (current == null)) {
        } else if ((previous != null)
                && (current != null)
                && (previous.equals(current))) {
        } else {
            this.queueEvent(new AreaSelectedEvent(this));
        }
    }

    @Override
    public String getFamily() {
        return ("Map");
    }
}
