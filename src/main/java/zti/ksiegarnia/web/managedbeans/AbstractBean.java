package zti.ksiegarnia.web.managedbeans;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class AbstractBean implements Serializable {

    private static final long serialVersionUID = -3375564172975657665L;
    @Inject
    ShoppingCart cart;

    protected FacesContext context() {
        return (FacesContext.getCurrentInstance());
    }

    protected void message(String clientId, String key) {
        String text;

        try {
            ResourceBundle bundle = ResourceBundle.getBundle(
                    "zti.ksiegarnia.web.messages.Messages",
                    context().getViewRoot().getLocale());
            text = bundle.getString(key);
        } catch (Exception e) {
            text = "???" + key + "???";
        }

        context().addMessage(clientId, new FacesMessage(text));
    }

    protected void message(String clientId, String key, Object[] params) {
        String text;

        try {
            ResourceBundle bundle = ResourceBundle.getBundle(
                    "zti.ksiegarnia.web.messages.Messages",
                    context().getViewRoot().getLocale());
            text = bundle.getString(key);
        } catch (Exception e) {
            text = "???" + key + "???";
        }

        if ((params != null) && (params.length > 0)) {
            text = MessageFormat.format(text, params);
        }

        context().addMessage(clientId, new FacesMessage(text));
    }
}
