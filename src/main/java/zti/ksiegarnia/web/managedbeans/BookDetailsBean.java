package zti.ksiegarnia.web.managedbeans;

import java.io.Serializable;
import zti.ksiegarnia.entity.Book;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named("details")
@SessionScoped
public class BookDetailsBean extends AbstractBean implements Serializable {

    private static final long serialVersionUID = 2209748452115843974L;

    public String add() {
        Book book = (Book) context().getExternalContext()
                .getSessionMap().get("selected");
        cart.add(book.getBookId(), book);
        message(null, "ConfirmAdd", new Object[]{book.getTitle()});

        return ("bookcatalog");
    }
}
