package zti.ksiegarnia.model;

import java.io.Serializable;

public final class ImageArea implements Serializable {

    private static final long serialVersionUID = -2382340811618738146L;
    private String alt = null;
    private String coords = null;
    private String shape = null;

    public ImageArea() {
    }

    public ImageArea(String alt, String coords, String shape) {
        setAlt(alt);
        setCoords(coords);
        setShape(shape);
    }

    public String getAlt() {
        return (this.alt);
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getCoords() {
        return (this.coords);
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }

    public String getShape() {
        return (this.shape);
    }

    public void setShape(String shape) {
        this.shape = shape;
    }
}
