package zti.ksiegarnia.exception;

public class BooksNotFoundException extends Exception {

    private static final long serialVersionUID = 4156679691884326238L;

    public BooksNotFoundException() {
    }

    public BooksNotFoundException(String msg) {
        super(msg);
    }
}
