package zti.ksiegarnia.exception;

public class OrderException extends Exception {

    private static final long serialVersionUID = -5058707185180716794L;

    public OrderException() {
    }

    public OrderException(String msg) {
        super(msg);
    }
}
