package zti.ksiegarnia.listeners;

import zti.ksiegarnia.components.MapComponent;
import javax.faces.event.ActionEvent;

public class AreaSelectedEvent extends ActionEvent {

    private static final long serialVersionUID = 5030325260451465110L;

    public AreaSelectedEvent(MapComponent map) {
        super(map);
    }

    public MapComponent getMapComponent() {
        return ((MapComponent) getComponent());
    }
}
