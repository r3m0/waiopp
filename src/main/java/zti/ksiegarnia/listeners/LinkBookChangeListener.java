package zti.ksiegarnia.listeners;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

public class LinkBookChangeListener implements ActionListener {

    private static final Logger logger =
            Logger.getLogger("ksiegarnia.listeners.LinkBookChangeListener");
    private HashMap<String, String> books = null;

    public LinkBookChangeListener() {
        books = new HashMap<>(6);

        String book1 = books.put("Walls", "201");
        String book2 = books.put("Martin", "202");
        String book3 = books.put("Robert", "203");
        String book4 = books.put("Loeliger", "205");
        String book5 = books.put("Hunt", "206");
        String book6 = books.put("Howard", "207");
    }

    @Override
    public void processAction(ActionEvent event)
            throws AbortProcessingException {
        logger.log(Level.INFO, "Entering LinkBookChangeListener.processAction");
        String current = event.getComponent().getId();
        FacesContext context = FacesContext.getCurrentInstance();
        String bookId = books.get(current);
        context.getExternalContext().getSessionMap().put("bookId", bookId);
    }
}
