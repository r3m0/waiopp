package zti.ksiegarnia.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("ccno")
public class CreditCardConverter implements Converter {

    public static final String CONVERSION_ERROR_MESSAGE_ID = "ConversionError";
    private static final Logger logger =
            Logger.getLogger("ksiegarnia.converters.CreditCardConverter");

    public CreditCardConverter() {
    }

    @Override
    public Object getAsObject(FacesContext context,
            UIComponent component, String newValue)
            throws ConverterException {

        logger.log(Level.INFO, "Entering CreditCardConverter.getAsObject");

        if (newValue.isEmpty()) {
            return null;
        }

        String convertedValue = newValue.trim();
        if ((convertedValue.contains("-")) || (convertedValue.contains(" "))) {

            char[] input = convertedValue.toCharArray();
            StringBuilder builder = new StringBuilder(input.length);

            for (int i = 0; i < input.length; ++i) {
                if ((input[i] == '-') || (input[i] == ' ')) {
                } else {
                    builder.append(input[i]);
                }
            }

            convertedValue = builder.toString();
        }
        logger.log(Level.INFO, "Converted value is {0}", convertedValue);
        return convertedValue;
    }

    @Override
    public String getAsString(FacesContext context,
            UIComponent component, Object value)
            throws ConverterException {

        String inputVal = null;

        logger.log(Level.INFO, "Entering CreditCardConverter.getAsString");

        if (value == null) {
            return "";
        }

        try {
            inputVal = (String) value;
        } catch (ClassCastException ce) {
            FacesMessage errMsg = new FacesMessage(CONVERSION_ERROR_MESSAGE_ID);
            FacesContext.getCurrentInstance().addMessage(null, errMsg);
            throw new ConverterException(errMsg.getSummary());
        }

        char[] input = inputVal.toCharArray();
        StringBuilder builder = new StringBuilder(input.length + 3);

        for (int i = 0; i < input.length; ++i) {
            if (((i % 4) == 0) && (i != 0)) {
                if ((input[i] != ' ') || (input[i] != '-')) {
                    builder.append(" ");

                } else if (input[i] == '-') {
                    builder.append(" ");
                }
            }

            builder.append(input[i]);
        }

        String convertedValue = builder.toString();
        logger.log(Level.INFO, "Converted value is {0}", convertedValue);

        return convertedValue;
    }
}
